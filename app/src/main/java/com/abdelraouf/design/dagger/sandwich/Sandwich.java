package com.abdelraouf.design.dagger.sandwich;

import android.util.Log;

import com.abdelraouf.design.dagger.HomeActivity;

public abstract  class Sandwich {
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	public void eat() {
		Log.v(HomeActivity.TAG, getClass().getSimpleName() + " hmmm");
	}
}
