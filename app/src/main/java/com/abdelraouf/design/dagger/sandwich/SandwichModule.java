package com.abdelraouf.design.dagger.sandwich;

import com.abdelraouf.design.dagger.butter.AlmondButter;
import com.abdelraouf.design.dagger.butter.CashewButter;

import dagger.Module;
import dagger.Provides;

@Module
public class SandwichModule {

	@Provides
	@SandwichScope
	CashewSandwich provideCashewSandwich(CashewButter butter){
		return new CashewSandwich(butter);
	}

	@Provides
	AlmondSandwich provideAlmondSandwich(AlmondButter butter){
		return new AlmondSandwich(butter);
	}
}
