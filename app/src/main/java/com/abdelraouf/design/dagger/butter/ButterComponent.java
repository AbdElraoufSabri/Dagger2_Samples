package com.abdelraouf.design.dagger.butter;

import dagger.Component;

@ButterScope
@Component(modules = ButterModule.class)
public interface ButterComponent {
	AlmondButter provideAlmondButter();
	CashewButter provideCashewButter();
}
